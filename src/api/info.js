import request from "@/utils/request";

export function getCategory(data) {
  return request.request({
    method: "POST",
    url: "/news/getCategory/",
    data
  });
}

//列表
export function getCategoryAll(data) {
  return request.request({
    method: "POST",
    url: "/news/getCategoryAll/",
    data
  });
}

//编辑
export function editCategory(data) {
  return request.request({
    method: "POST",
    url: "/news/editCategory/",
    data
  });
}

//删除
export function deleteCategory(data) {
  return request.request({
    method: "POST",
    url: "/news/deleteCategory/",
    data
  });
}

//添加一级分类
export function addFirstCategory(data) {
  return request.request({
    method: "POST",
    url: "/news/addFirstCategory/",
    data
  });
}

//添加二级分类
export function addSecondCategory(data) {
  return request.request({
    method: "POST",
    url: "/news/addChildrenCategory/",
    data
  });
}

//添加信息
export function addInfo(data) {
  return request.request({
    method: "POST",
    url: "/news/add/",
    data
  });
}

//删除信息
export function deleteInfo(data) {
  return request.request({
    method: "POST",
    url: "/news/deleteInfo/",
    data
  });
}

//获取信息列表
export function getInfoList(data) {
  return request.request({
    method: "POST",
    url: "/news/getList/",
    data
  });
}


