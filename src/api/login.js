import request from "@/utils/request";

//获取验证码
export function getSms(data) {
  return request.request({
    method: "POST",
    url: "/getSms/",
    data
  });
}

//注册
export function register(data) {
  return request.request({
    method: "POST",
    url: "/register/",
    data
  });
}

//登录
export function login(data) {
  return request.request({
    method: "POST",
    url: "/login/",
    data
  });
}
