import router from "./index";
import { getToken } from "../utils/app";

const whiteRoutes = ["/login"];

router.beforeEach((to, from, next) => {
  if (getToken()) {
    if (whiteRoutes.includes(to.fullPath)) {
      next("/index");
    } else {
      next();
    }
  } else {
    if (whiteRoutes.includes(to.fullPath)) {
      next();
    } else {
      next("/login");
    }
  }
});
