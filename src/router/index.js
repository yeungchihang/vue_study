import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Layout from "../views/Index.vue";

const routes = [
  {
    path: "/",
    redirect: "/login"
  },
  {
    component: () => import("../views/Login.vue"),
    path: "/login"
  },
  {
    component: Layout,
    path: "/index",
    meta: {
      title: "首页",
      icon: "el-icon-eleme"
    },
    children: [
      {
        component: () => import("../views/Login.vue"),
        path: "/i",
        meta: {
          title: "首页",
          icon: "el-icon-mobile-phone"
        }
      },
    ]
  },
  {
    component: Layout,
    path: "/info",
    meta: {
      title: "信息管理",
      icon: "el-icon-message"
    },
    children: [
      {
        component: () => import("../views/info/Index.vue"),
        path: "/infoIndex",
        meta: {
          title: "信息列表",
          icon: "el-icon-chat-dot-square"
        }
      },
      {
        component: () => import("../views/info/Category.vue"),
        path: "/infoCategory",
        meta: {
          title: "信息分类",
          icon: "el-icon-more"
        }
      },
      {
        component: () => import("../views/info/Index.vue"),
        path: "/infoDetail",
        meta: {
          title: "信息详情",
          icon: "el-icon-more",
          hidden: true
        }
      },
    ]
  },
  {
    component: Layout,
    path: "/User",
    meta: {
      title: "用户管理",
      icon: "el-icon-user"
    },
    children: [
      {
        component: () => import("../views/user/Index.vue"),
        path: "/UserIndex",
        meta: {
          title: "用户列表",
          icon: "el-icon-s-grid"
        }
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
