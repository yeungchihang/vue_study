import cookie from "cookie_js";

export function getToken() {
  return cookie.get("admin_token");
}

export function setToken(token) {
  return cookie.set("admin_token", token);
}

export function removeToken() {
  return cookie.remove("admin_token");
}

export function getUsername() {
  return cookie.get("username");
}

export function setUsername(username) {
  return cookie.set("username", username);
}

export function removeUsername() {
  return cookie.remove("username");
}
