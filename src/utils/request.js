import axios from "axios";
import { getToken, getUsername } from "./app";

// 创建axios实例
const service = axios.create({
  baseURL: "/api", // api的base_url
  timeout: 2000 // 请求超时时间2
});
// request拦截器
service.interceptors.request.use(
  config => {
    config.headers["ToKey"] = getToken();
    config.headers["Username"] = getUsername();
    return config;
  },
  error => {
    // Do something with request error
    console.error(error); // for debug
    Promise.reject(error);
  }
);
// respone拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.resCode === 0) {
      return res;
    } else {
      return Promise.reject(res);
    }
  },
  error => {
    console.error("err" + error); // for debug
    return Promise.reject(error);
  }
);

export default service;
