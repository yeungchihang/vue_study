export function formatDate(cellVal) {
  // 获取单元格数据
  if (!cellVal) {
    return "";
  }
  let dt = new Date(cellVal * 1000);
  return (
    dt.getFullYear() +
    "-" +
    (dt.getMonth() + 1) +
    "-" +
    dt.getDate() +
    " " +
    dt.getHours() +
    ":" +
    dt.getMinutes() +
    ":" +
    dt.getSeconds()
  );
}
