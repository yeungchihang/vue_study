export function validateInput(rule, value, callback){
    if (value === '') {
        callback(new Error(rule.field + '不能为空'));
    }
    callback();
};