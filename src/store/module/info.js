import { getCategory, getCategoryAll } from "@/api/info";

const state = {
  category: [],
  categoryAll: []
};

const getters = {
  category(state) {
    return state.category.map(item => {
      item.label = item.id + " - " + item.category_name;
      return item;
    });
  },
  categoryAll(state) {
    return state.categoryAll.map(item => {
      item.label = item.id + " - " + item.category_name;
      if (item.children != undefined) {
        item.children.forEach(child => {
          child.label = child.id + " - " + child.category_name;
        });
      }
      return item;
    });
  }
};

const mutations = {
  SET_CATEGORY(state, category) {
    state.category = category;
  },
  SET_CATEGORYALL(state, categoryAll) {
    state.categoryAll = categoryAll;
  }
};

const actions = {
  getCategory(context) {
    return new Promise(resolve => {
      getCategory().then(res => {
        context.commit("SET_CATEGORY", res.data.data);
        resolve(res);
      });
    });
  },
  getCategoryAll(context) {
    return new Promise(resolve => {
      getCategoryAll().then(res => {
        context.commit("SET_CATEGORYALL", res.data);
        resolve(res);
      });
    });
  }
};

export default { state, getters, mutations, actions };
