import Vue from "vue";
import Vuex from "vuex";
import login from "./module/login";
import info from "./module/info";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { login, info }
});
